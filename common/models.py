from django.db import models
from tagulous import models as tagulous_models


# Create your models here.
from myaudioshare.users.models import User


class Tags(tagulous_models.TagTreeModel):
    class TagMeta:
        force_lowercase = True
        # TODO: define autocomplete view, e.g.:
        # autocomplete_view = 'food.views.tag_autocomplete'


class CommonPropertiesModel(models.Model):
    name = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        User,
        related_name='%(app_label)s_%(class)s_created_by',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    updated_date = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(
        User,
        related_name='%(app_label)s_%(class)s_updated_by',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    tags = tagulous_models.TagField(to=Tags)
    active = models.BooleanField(default=True)

    def __iter__(self):
        field_names = [(f.name, f.verbose_name) for f in self._meta.fields]
        field_names += [(f.name, f.verbose_name) for f in self._meta.many_to_many]
        field_names = [(f.name, f.verbose_name, f,) for f in self._meta.fields]
        field_names += [(f.name, f.verbose_name, f,) for f in self._meta.many_to_many]
        for field_name in field_names:
            value = getattr(self, field_name[0], None)
            if isinstance(field_name[2], models.ManyToManyField):
                vs = []
                for v in value.all():
                    vs.append(v)
                value = vs
            yield (field_name[1], value)

    class Meta:
        abstract = True
