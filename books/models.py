from django.db import models
from positions import PositionField


# Create your models here.
from common.models import CommonPropertiesModel


class Narrator(CommonPropertiesModel):
    name = models.CharField(max_length=50)


class Author(CommonPropertiesModel):
    books = models.ManyToManyField('Book', blank=True)


class Series(CommonPropertiesModel):
    description = models.TextField(blank=True, null=True)


class Book(CommonPropertiesModel):
    name = None
    title = models.CharField(max_length=100)
    description = models.TextField()
    book_number = PositionField(unique_for_field='series')
    narrators = models.ManyToManyField(Narrator)
    series = models.ForeignKey(Series, on_delete=models.PROTECT)
    public = models.BooleanField(default=True)


class Image(CommonPropertiesModel):
    name = None
    tags = None
    image = models.ImageField(upload_to="Images")
    book = models.ForeignKey(Book, on_delete=models.CASCADE)


class AudioFile(CommonPropertiesModel):
    name = None
    tags = None
    audio_file = models.FileField(upload_to='AudioFiles')
    book = models.ForeignKey(Book, on_delete=models.CASCADE)


class Genre(CommonPropertiesModel):
    description = models.TextField(blank=True, null=True)
    books = models.ManyToManyField(Book, blank=True)
