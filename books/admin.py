from django.contrib import admin

# Register your models here.
from books.models import Book, Narrator, Author, Series, Image, AudioFile, Genre


class ImageInline(admin.StackedInline):
    model = Image
    extra = 0


class AudioFileInline(admin.StackedInline):
    model = AudioFile
    extra = 0


class BookAdmin(admin.ModelAdmin):
    inlines = (ImageInline, AudioFileInline,)
    # exclude = ('address',)


admin.site.register(Narrator)
admin.site.register(Author)
admin.site.register(Series)
admin.site.register(Book, BookAdmin)
admin.site.register(Image)
admin.site.register(AudioFile)
admin.site.register(Genre)
